#!/bin/bash

## DO NOT USE THIS !!!   -- this is a process to use in a local dev envinroment to remove and reinstall all composer requirements  only for  investigation purpose


##  reset installation - this is not necessary but it help me to reset my situation during my experiments
echo '## reset previous attempt ##'
git checkout -- composer.lock
git checkout -- composer.json
rm -rf vendor


## parse composer.json file and generate a bash script to remove all requirements and another script to require them back 
remove_requires=`php << 'EOF'
<?php echo 'composer config --no-plugins allow-plugins.composer/installers true '. "\n" ?>
<?php echo 'composer config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true '. "\n" ?>
<?php echo 'COMPOSER_DISCARD_CHANGES=true '. "\n" ?>
<?php foreach(json_decode(file_get_contents('composer.json'),true)['require'] as $k => $v){echo 'composer remove --no-interaction ' . $k . "\n";} ?>
<?php foreach(json_decode(file_get_contents('composer.json'),true)['require-dev'] as $k => $v){echo 'composer remove --no-interaction ' . $k . "\n";} ?>
EOF`
echo "$remove_requires" > remove-requires.sh
chmod +x remove-requires.sh


reinstall_all_requires=`php << 'EOF'
<?php echo 'composer config --no-plugins allow-plugins.composer/installers true '. "\n" ?>
<?php echo 'composer config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true '. "\n" ?>
<?php echo 'COMPOSER_DISCARD_CHANGES=true '. "\n" ?>
<?php foreach(json_decode(file_get_contents('composer.json'),true)['require'] as $k => $v){echo 'composer require -W --no-interaction ' . $k . "\n";} ?>
<?php foreach(json_decode(file_get_contents('composer.json'),true)['require-dev'] as $k => $v){echo 'composer require -W --no-interaction --dev ' . $k . "\n";} ?>
EOF`
echo "$reinstall_all_requires" > reinstall-all-requires.sh
chmod +x reinstall-all-requires.sh


## removing all requirements
./remove-requires.sh

exit


echo "#### INSTALLING SECTOR 10 ####"
composer require -W  sparksinteractive/sector-distribution 10.0.x@dev


echo "#### CONFIGURE AND ADD LENIENT ###"
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/better_normalizers"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/wysiwyg_template"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/wysiwyg_template_core"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/sector_starter"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/sector_portfolio"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/sector_blog"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/core"]'
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/sector_9to10_legacy_pack 9.5.x-dev"]'
composer config --no-plugins allow-plugins.mglaman/composer-drupal-lenient true
composer require mglaman/composer-drupal-lenient

echo "#### INSTALLING legacy modules ####"
composer require -W  drupal/sector_9to10_legacy_pack 9.5.x-dev


echo '### reinstalling the packages prevuiously removed####'


composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/entity_reference_hierarchy"]'


## reinstalling all previous requirements 
./reinstall-all-requires.sh
